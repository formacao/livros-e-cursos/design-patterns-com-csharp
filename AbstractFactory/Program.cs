﻿using System;
using AbstractFactory.Fábricas;

namespace AbstractFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Terran | Protoss | Zerg");
            Console.WriteLine();

            var escolha = Console.ReadLine();
            
            new BaseFactoryMethod()
                .RetornarFábrica(escolha);
            
        }
    }
}