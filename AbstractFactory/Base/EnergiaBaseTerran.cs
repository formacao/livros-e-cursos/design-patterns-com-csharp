﻿using System;
using System.Xml;
using AbstractFactory.Base.Interfaces;

namespace AbstractFactory.Base
{
    public class EnergiaBaseTerran : IEnergia
    {
        public void Composição()
        {
            Console.WriteLine("Energia Mecânica");
        }
    }
}