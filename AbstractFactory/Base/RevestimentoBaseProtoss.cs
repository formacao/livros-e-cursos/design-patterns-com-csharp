﻿using System;
using AbstractFactory.Base.Interfaces;

namespace AbstractFactory.Base
{
    public class RevestimentoBaseProtoss : IRevestimento
    {
        public void Composição()
        {
            Console.WriteLine("Revestimento Amarelo");
        }
    }
}