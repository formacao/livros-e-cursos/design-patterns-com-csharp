﻿namespace AbstractFactory.Base.Interfaces
{
    public interface IRevestimento
    {
        void Composição();
    }
}