﻿using System;
using AbstractFactory.Base.Interfaces;

namespace AbstractFactory.Base
{
    public class RevestimentoBaseTerran : IRevestimento
    {
        public void Composição()
        {
            Console.WriteLine("Revestimento verde");
        }
    }
}