﻿using System;
using AbstractFactory.Base.Interfaces;

namespace AbstractFactory.Base
{
    public class RevestimentoBaseZerg : IRevestimento
    {
        public void Composição()
        {
            Console.WriteLine("Revestimento Vermelho");
        }
    }
}