﻿using System;
using AbstractFactory.Base.Interfaces;

namespace AbstractFactory.Base
{
    public class EnergiaBaseZerg: IEnergia
    {
        public void Composição()
        {
            Console.WriteLine("Energia da Terra");
        }
    }
}