﻿using System;
using AbstractFactory.Base.Interfaces;

namespace AbstractFactory.Base
{
    public class EnergiaBaseProtoss : IEnergia
    {
        public void Composição()
        {
            Console.WriteLine("Energia dos Cristais");
        }
    }
}