﻿using System;
using AbstractFactory.Base;
using AbstractFactory.Fábricas.Interfaces;

namespace AbstractFactory.Fábricas
{
    public class FábricaBaseProtoss : IFábricaBases
    {
        public FábricaBaseProtoss()
        {
            Fabricar();
        }
        public void Fabricar()
        {
            Console.WriteLine("Base Protoss criada com sucesso");
            new EnergiaBaseProtoss().Composição();
            new RevestimentoBaseProtoss().Composição();
        }
    }
}