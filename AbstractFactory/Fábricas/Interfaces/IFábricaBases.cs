﻿namespace AbstractFactory.Fábricas.Interfaces
{
    public interface IFábricaBases
    {
        void Fabricar();
    }
}