﻿using System;
using AbstractFactory.Base;
using AbstractFactory.Fábricas.Interfaces;

namespace AbstractFactory.Fábricas
{
    public class FábricaBaseZerg : IFábricaBases
    {
        public FábricaBaseZerg()
        {
            Fabricar();
        }
        public void Fabricar()
        {
            Console.WriteLine("Base Zerg criada com sucesso");
            new EnergiaBaseZerg().Composição();
            new RevestimentoBaseZerg().Composição();
        }
    }
}