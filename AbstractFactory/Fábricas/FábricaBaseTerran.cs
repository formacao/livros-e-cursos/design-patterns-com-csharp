﻿using System;
using AbstractFactory.Base;
using AbstractFactory.Fábricas.Interfaces;

namespace AbstractFactory.Fábricas
{
    public class FábricaBaseTerran : IFábricaBases
    {
        public FábricaBaseTerran()
        {
            Fabricar();
        }
        public void Fabricar()
        {
            Console.WriteLine("Base Terran criada com sucesso");
            new EnergiaBaseTerran().Composição();
            new RevestimentoBaseTerran().Composição();
        }
    }
}