﻿using System;
using AbstractFactory.Fábricas.Interfaces;

namespace AbstractFactory.Fábricas
{
    public class BaseFactoryMethod
    {
        public IFábricaBases RetornarFábrica(string nome) => nome switch
        {
            "Terran" => new FábricaBaseTerran(),
            "Protoss" => new FábricaBaseProtoss(),
            "Zerg" => new FábricaBaseZerg(),
            _ => throw new InvalidOperationException("Raça Inexistente")
        };
    }
}