﻿using System;

namespace Visistor
{
    public class NívelVisitor : IVisitor
    {
        public void Identificar(Chefão chefão)
        {
            var nívelDificuldade = chefão.Nome switch
            {
                "Boss 1" => 25,
                "Boss 2" => 50
            };
            
            Console.WriteLine("O chefão {0} é {1}% difícil e tem {2} pontos de vida", chefão.Nome, nívelDificuldade, chefão.Vida);
        }

        public void Identificar(FaseJogo faseJogo)
        {
            var nívelDificuldade = faseJogo.Nome switch
            {
                "Floresta" => 70,
                "Caverna" => 30
            };
            
            Console.WriteLine("A fase {0} no jogo é {1}% difícil", faseJogo.Nome, nívelDificuldade);
        }
    }
}