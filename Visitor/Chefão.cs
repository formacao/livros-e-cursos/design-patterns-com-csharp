﻿namespace Visistor
{
    public class Chefão : IJogo
    {
        public string Nome { get; }
        public int Vida { get; }

        public Chefão(string nome, int vida)
        {
            Nome = nome;
            Vida = vida;
        }

        public void Visitante(IVisitor visitante) => visitante.Identificar(this);
    }
}