﻿namespace Visistor
{
    public class FaseJogo : IJogo
    {
        public string Nome { get;  }
        public FaseJogo(string nome)
        {
            Nome = nome;
        }

        public void Visitante(IVisitor visitante) => visitante.Identificar(this);
        
    }
}