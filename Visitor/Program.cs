﻿using System;
using System.Collections.Generic;

namespace Visistor
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IJogo> jogo = new List<IJogo>();
            jogo.Add(new FaseJogo("Floresta"));
            jogo.Add(new FaseJogo("Caverna"));
            
            jogo.Add(new Chefão("Boss 1", 30));
            jogo.Add(new Chefão("Boss 2", 50));

            var nívelVisitor = new NívelVisitor();
            jogo.ForEach(x => x.Visitante(nívelVisitor));

            Console.ReadLine();
        }
    }
}