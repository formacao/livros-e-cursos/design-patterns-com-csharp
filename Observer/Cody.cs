﻿using System.Collections.Generic;

namespace Observer
{
    public class Cody : IPersonagem
    {
        private List<IObservador> _inimigos = new List<IObservador>();

        public int Vida { get; private set; }

        public Cody()
        {
            Vida = 100;
        }
        
        public void RegistrarObservador(IObservador observador) => _inimigos.Add(observador);
        public void NotificarPersonagens() => _inimigos.ForEach(x => x.Avisar(this));

        public void LevarGolpe()
        {
            Vida -= 10;
            NotificarPersonagens();
        }
    }
}