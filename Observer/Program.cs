﻿using System;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            var cody = new Cody();

            var inimigo1 = new Inimigo(cody);
            var inimigo2 = new Inimigo(cody);
            var inimigo3 = new Inimigo(cody);
            
            cody.RegistrarObservador(inimigo1);
            cody.RegistrarObservador(inimigo2);
            cody.RegistrarObservador(inimigo3);

            string ação;
           do
            {
                Console.WriteLine("Acertar o Cody? (S/N) (Qualquer outro para sair)");
                ação = Console.ReadLine();
                switch (ação)
                {
                    case "S": 
                        cody.LevarGolpe();
                        break;
                    case "N":
                        Console.WriteLine("Você não acertou golpe.");
                        break;
                }
            }while ("S".Equals(ação) || "N".Equals(ação)) ;
           
        }
    }
}