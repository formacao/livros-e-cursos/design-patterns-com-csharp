﻿using System;

namespace Observer
{
    public class Inimigo : IObservador
    {
        private Cody _personagemObservado;

        public Inimigo(Cody personagemObservado)
        {
            _personagemObservado = personagemObservado;
        }

        public void Avisar(IPersonagem personagem)
        {
            if (personagem != _personagemObservado) return;
            Console.WriteLine($"O Cody foi acertado com um golpe, agora sua vida de jogo é de: {_personagemObservado.Vida}.");
        }
    }
}