﻿using System;
using System.Text;

namespace Composite
{
    class Program
    {
        static void Main(string[] args)
        {
            var mapa = new FaseComposite("Mapa das Cavernas");

            var caverna1 = new FaseComposite("Caverna 1");
            var caverna2 = new FaseComposite("Caverna 2");

            for (var i = 0; i < 3; i++) AdicionarFase(caverna1, i);
            for (var i = 3; i < 5; i++) AdicionarFase(caverna2, i);

            var portaSecreta = new FaseComposite("Porta Secreta");
            AdicionarFase(portaSecreta, "Subfase Secreta");
            
            mapa.Adicionar(caverna1);
            mapa.Adicionar(caverna2);
            caverna2.Adicionar(portaSecreta);
            
            mapa.Mostrar(1);

            Console.ReadKey();
        }

        private static void AdicionarFase(ComponenteFase fase, int índice)
        {
            var nomeFase = new StringBuilder()
                .Append("Subfase ")
                .Append("0")
                .Append(índice + 1)
                .ToString();

            AdicionarFase(fase, nomeFase);
        }

        private static void AdicionarFase(ComponenteFase fase, string nomeFase)
        {
            fase.Adicionar(new FaseJogo(nomeFase));
        }
    }
}