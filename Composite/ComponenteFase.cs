﻿using System;

namespace Composite
{
    public abstract  class ComponenteFase
    {
        protected string _nome;

        protected ComponenteFase(string nome)
        {
            _nome = nome;
        }

        public abstract void Adicionar(ComponenteFase componente);
        public abstract void Remover(ComponenteFase componente);

        public virtual void Mostrar(int profundidade)
        {
            var dashes = new string('-', profundidade);
            Console.WriteLine($"{dashes}  {_nome}");        
        }
    }
}