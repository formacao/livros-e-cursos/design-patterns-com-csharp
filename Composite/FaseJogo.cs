﻿using System;

namespace Composite
{
    public class FaseJogo : ComponenteFase
    {
        public FaseJogo(string nome) : base(nome)
        {
        }

        public override void Adicionar(ComponenteFase componente)
        {
            Console.WriteLine("Não é possível adicionar fase no jogo por aqui");
        }

        public override void Remover(ComponenteFase componente)
        {
            Console.WriteLine("Não é possível remover fase no jogo por aqui");
        }
    }
}