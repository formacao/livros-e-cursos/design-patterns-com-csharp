﻿using System.Collections.Generic;

namespace Composite
{
    public class FaseComposite : ComponenteFase
    {
        private List<ComponenteFase> _fasesJogo = new List<ComponenteFase>();

        public FaseComposite(string nome) : base(nome)
        {
        }

        public override void Adicionar(ComponenteFase componente)
        {
            _fasesJogo.Add(componente);
        }

        public override void Remover(ComponenteFase componente)
        {
            _fasesJogo.Remove(componente);
        }

        public override void Mostrar(int profundidade)
        {
            base.Mostrar(profundidade);
            _fasesJogo.ForEach(x => x.Mostrar(profundidade + 2));
        }
    }
}