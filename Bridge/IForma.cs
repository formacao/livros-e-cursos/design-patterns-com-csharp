﻿namespace Bridge
{
    public interface IForma
    {
        ICor Cor { get; set; }
        
        string Descer();

        public static int QuantidadeFormas => 2;

    }
}