﻿namespace Bridge
{
    public class Forma1 : IForma
    {
        public ICor Cor { get; set; }
        public string Descer() => $"T - {Cor.Cor}";
    }
}