﻿using System;

namespace Bridge
{
    class Program
    {
        static void Main(string[] args)
        {
            var bridge = new FormaCorBridge();

            var forma = bridge.GerarForma();
            Console.WriteLine(forma.Descer());
        }
    }
}