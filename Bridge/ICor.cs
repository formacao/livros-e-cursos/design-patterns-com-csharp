﻿namespace Bridge
{
    public interface ICor
    {
        string Cor { get; }

        public static int QuantidadeCores => 3;
    }
}