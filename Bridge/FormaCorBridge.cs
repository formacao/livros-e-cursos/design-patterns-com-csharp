﻿using System;

namespace Bridge
{
    public class FormaCorBridge
    {
        public IForma GerarForma(Type tForma, Type tCor)
        {
            var cor = (ICor)Activator.CreateInstance(tCor);
            var forma = (IForma)Activator.CreateInstance(tForma);

            forma.Cor = cor;
            return forma;
        }
        
        public IForma GerarForma()
        {
            var random = new Random();

            var tForma = random.Next(IForma.QuantidadeFormas) switch
            {
                0 => typeof(Forma1),
                1 => typeof(Forma2),
                _ => typeof(Forma1)
            };

            var tCor = random.Next(ICor.QuantidadeCores) switch
            {
                0 => typeof(Laranja),
                1 => typeof(Verde),
                2 => typeof(Rosa),
                _ => typeof(Laranja)
            };

            return GerarForma(tForma, tCor);
        }
    }
}