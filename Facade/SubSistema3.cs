﻿using System;

namespace Facade
{
    public class SubSistema3
    {
        public void Responsabilidade() => Console.WriteLine("Produzir armamento para guerreiros");
    }
}