﻿using System;

namespace Facade
{
    class Program
    {
        static void Main(string[] args)
        {
            var facade = new SubSistemaFacade();
            
            facade.OperaçãoA();
            facade.OperaçãoB();

            Console.ReadKey();
        }
    }
}