﻿using System;

namespace Facade
{
    public class SubSistema2
    {
        public void Responsabilidade() => Console.WriteLine("Produzir armamento para guerreiros");
    }
}