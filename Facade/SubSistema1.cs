﻿using System;

namespace Facade
{
    public class SubSistema1
    {
        public void Responsabilidade() => Console.WriteLine("Coletar recursos de energia para a base");
    }
}