﻿using System;

namespace Facade
{
    public class SubSistemaFacade
    {
        private readonly SubSistema1 _subSistema1;
        private readonly SubSistema2 _subSistema2;
        private readonly SubSistema3 _subSistema3;


        public SubSistemaFacade()
        {
            _subSistema1 = new SubSistema1();
            _subSistema2 = new SubSistema2();
            _subSistema3 = new SubSistema3();
        }

        public void OperaçãoA()
        {
            Console.WriteLine("====Operação A====");
            _subSistema1.Responsabilidade();
            _subSistema2.Responsabilidade();
        }

        public void OperaçãoB()
        {
            Console.WriteLine("====Operação B====");
            _subSistema3.Responsabilidade();
        }
    }
}