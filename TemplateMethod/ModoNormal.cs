﻿using System;

namespace TemplateMethod
{
    public class ModoNormal : Jogo
    {
        protected override void PrimeiraFase()
        {
            Console.WriteLine("Carro deve abastecer uma vez");
        }

        protected override void SegundaFase()
        {
            Console.WriteLine("Carros adversários correm na mesma velocidade");
        }
    }
}