﻿using System;

namespace TemplateMethod
{
    public abstract class Jogo
    {
        protected Jogo()
        {
            TrilhaSonora();
            PrimeiraFase();
            SegundaFase();
        }

        protected abstract void PrimeiraFase();
        protected abstract void SegundaFase();

        private void TrilhaSonora()
        {
            Console.WriteLine("Música emocionante");
        }
    }
}