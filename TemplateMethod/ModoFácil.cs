﻿using System;

namespace TemplateMethod
{
    public class ModoFácil : Jogo
    {
        protected override void PrimeiraFase()
        {
            Console.WriteLine("Combustível para a corrida toda");
        }

        protected override void SegundaFase()
        {
            Console.WriteLine("Carros adversários devem correr menos");
        }
    }
}