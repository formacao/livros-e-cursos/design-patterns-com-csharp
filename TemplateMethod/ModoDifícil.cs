﻿using System;

namespace TemplateMethod
{
    public class ModoDifícil : Jogo
    {
        protected override void PrimeiraFase()
        {
            Console.WriteLine("Adicionar obstáculo na pista");
        }

        protected override void SegundaFase()
        {
            Console.WriteLine("Carros adversários devem correr mais");
        }
    }
}