﻿using System;

namespace TemplateMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("###### Escolha o modo de corrida ######");
            Console.WriteLine("1 - Fácil | 2 - Normal | 3 - Difícil");
            Console.WriteLine("Suas condições de jogo são:");

            Jogo jogo = Console.ReadLine() switch
            {
                "1" => new ModoFácil(),
                "2" => new ModoNormal(),
                "3" => new ModoDifícil(),
                _ => new ModoDifícil()
            };

            Console.ReadKey();
        }
    }
}