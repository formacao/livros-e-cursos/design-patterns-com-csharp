﻿using System;

namespace Mediator
{
    class Program
    {
        static void Main(string[] args)
        {
            var mediador = new MediadorConcreto();

            var j1 = new Jogador1(mediador);
            var j2 = new Jogador2(mediador);

            mediador.Jogador1 = j1;
            mediador.Jogador2 = j2;
            
            j1.Enviar("Essa partida foi boa!");
            j2.Enviar("Foi mesmo");

            Console.ReadKey();
        }
    }
}