﻿using System;

namespace Mediator
{
    public abstract  class Jogador
    {
        protected Mediador _mediador;

        protected Jogador(Mediador mediador)
        {
            _mediador = mediador;
        }

        public void Enviar(string mensagem)
        {
            _mediador.Enviar(mensagem, GetType());
        }

        protected string MontarMensagem(string nomeJogador, string mensagem) => $"Mensagem do {nomeJogador}: {mensagem}";
        public abstract void Notificar(string mensagem);
    }
}