﻿using System;

namespace Mediator
{
    public class Jogador1 : Jogador
    {
        public Jogador1(Mediador mediador) : base(mediador)
        {
        }


        public override void Notificar(string mensagem) => Console.WriteLine(MontarMensagem("Jogador 1", mensagem));
    }
}