﻿using System;

namespace Mediator
{
    public class Jogador2 : Jogador
    {
        public Jogador2(Mediador mediador) : base(mediador)
        {
        }

        public override void Notificar(string mensagem) => Console.WriteLine(MontarMensagem("Jogador 2", mensagem));
    }
}