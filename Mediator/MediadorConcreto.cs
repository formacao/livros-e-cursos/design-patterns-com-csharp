﻿using System;

namespace Mediator
{
    public class MediadorConcreto : Mediador
    {

        public Jogador Jogador1 { get; set; }
        public Jogador  Jogador2 { get; set; }

        public override void Enviar(string mensagem, Type jogador)
        {
            var instânciaJogador = typeof(Jogador1) == jogador ? Jogador2 : Jogador1;
            instânciaJogador.Notificar(mensagem);
        }
    }
}