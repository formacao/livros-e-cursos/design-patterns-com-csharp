﻿using System;

namespace Adapter
{
    public class Avião
    {
        public void Voar(string personagem)
        {
            Console.WriteLine($"{personagem} voou para frente!");
        }

        public void SoltarMíssil()
        {
            Console.WriteLine("Soltou o míssil no jogo!");
        }
    }
}