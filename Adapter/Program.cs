﻿using System;

namespace Adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            var rodrigo = new Personagem();
            var avião = new Avião();
            var adaptadorAvião = new Adapter(avião);
            
            Console.WriteLine("-------CAMINHANDO--------");
            rodrigo.Andar("Rodrigo");
            rodrigo.Atirar();
            
            Console.WriteLine();
            
            Console.WriteLine("--------PEGOU O AVIÃO---------");
            adaptadorAvião.Andar("Rodrigo");
            adaptadorAvião.Atirar();
        }
    }
}