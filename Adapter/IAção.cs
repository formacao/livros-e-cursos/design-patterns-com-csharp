﻿namespace Adapter
{
    public interface IAção
    {
        void Andar(string jogador);
        void Atirar();
    }
}