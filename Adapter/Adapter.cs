﻿namespace Adapter
{
    public class Adapter : IAção
    {
        public Avião Avião { get; }
        
        public Adapter(Avião avião)
        {
            Avião = avião;
        }
        
        public void Andar(string jogador)
        {
            Avião.Voar(jogador);
        }

        public void Atirar()
        {
            Avião.SoltarMíssil();
        }
    }
}