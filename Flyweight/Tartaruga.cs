﻿using System;

namespace Flyweight
{
    public abstract class Tartaruga
    {
        protected string Condição;
        protected string Ação;
        
        public string Cor { get; set; }

        public virtual void Mostrar(string cor)
        {
            Cor = cor;
            Console.WriteLine($"{Condição} {Ação} {Cor.ToUpper()}");
        }
    }
}