﻿using System;

namespace Flyweight
{
    public class Vermelha : Tartaruga
    {
        public Vermelha()
        {
            Condição = "Tartaruga dentro do casco";
            Ação = "Rodando";
        }

    }
}