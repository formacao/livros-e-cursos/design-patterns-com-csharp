﻿using System;

namespace Flyweight
{
    class Program
    {
        static void Main(string[] args)
        {
            var tartarugaFlyweight = new TartarugaFlyweight();

            while (true)
            {
                Console.WriteLine("Qual tartaruga enviar para tela?");
                var cor = Console.ReadLine();
                if (string.IsNullOrEmpty(cor) || string.IsNullOrWhiteSpace(cor)) break;
                
                var tartaruga = tartarugaFlyweight[cor];
                tartaruga.Mostrar(cor);
                
                Console.WriteLine();
                Console.WriteLine("--------------------------------");
            }
        }
    }
}