﻿namespace Flyweight
{
    public class Verde : Tartaruga
    {
        public Verde()
        {
            Ação = "Atirar";
            Condição = "Armada";
        }
    }
}