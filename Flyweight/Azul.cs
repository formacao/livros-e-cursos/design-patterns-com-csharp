﻿namespace Flyweight
{
    public class Azul : Tartaruga
    {
        public Azul()
        {
            Ação = "Correr";
            Condição = "Fora do casco";
        }
    }
}