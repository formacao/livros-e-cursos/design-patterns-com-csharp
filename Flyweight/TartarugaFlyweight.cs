﻿using System;
using System.Collections.Generic;

namespace Flyweight
{
    public class TartarugaFlyweight
    {
        private Dictionary<string, Tartaruga> _tartarugas = new Dictionary<string, Tartaruga>();

        public Tartaruga this[string cor] => GetTartaruga(cor);

        private Tartaruga GetTartaruga(string cor)
        {
            if (_tartarugas.ContainsKey(cor)) return _tartarugas[cor];

            _tartarugas[cor] = cor switch
            {
                "azul" => new Azul(),
                "verde" => new Verde(),
                "laranja" => new Laranja(),
                "vermelha" => new Vermelha(),
                _ => new TartarugaPadrão()
            };

            return _tartarugas[cor];
        }
    }
}