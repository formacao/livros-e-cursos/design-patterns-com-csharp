﻿namespace Flyweight
{
    public class Laranja : Tartaruga
    {
        public Laranja()
        {
            Ação = "Pular";
            Condição = "Dentro do casco";
        }   
    }
}