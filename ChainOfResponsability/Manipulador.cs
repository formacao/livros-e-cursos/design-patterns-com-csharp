﻿namespace ChainOfResponsability
{
    public abstract  class Manipulador
    {
        protected Manipulador _sucessor;

        public void DefinirSucessor(Manipulador sucessor) => _sucessor = sucessor;
        public abstract void Convocar(int quantidadePoder);
    }
}