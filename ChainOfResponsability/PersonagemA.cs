﻿using System;

namespace ChainOfResponsability
{
    public class PersonagemA : Manipulador
    {
        public override void Convocar(int quantidadePoder)
        {
            if(quantidadePoder >= 0 && quantidadePoder < 10) 
                Console.WriteLine("{0} convocado para uma força  de por de {1}", GetType().Name, quantidadePoder);
            else if(_sucessor != null)
                _sucessor.Convocar(quantidadePoder);
        }
    }
}