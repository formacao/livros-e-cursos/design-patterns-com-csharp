﻿using System;

namespace ChainOfResponsability
{
    public class PersonagemB : Manipulador
    {
        public override void Convocar(int quantidadePoder)
        {
            if(quantidadePoder >= 10 && quantidadePoder < 20)
                Console.WriteLine("{0} convocado para uma força de poder de {1}", GetType().Name, quantidadePoder);
            else if (_sucessor != null)
                _sucessor.Convocar(quantidadePoder);
        }
    }
}