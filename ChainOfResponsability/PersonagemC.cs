﻿using System;

namespace ChainOfResponsability
{
    public class PersonagemC : Manipulador
    {
        public override void Convocar(int quantidadePoder)
        {
            if(quantidadePoder >= 20 && quantidadePoder < 30)
                Console.WriteLine("{0} convocado para uma força de poder de {1}", GetType().Name, quantidadePoder);
            else if(_sucessor != null)
                _sucessor.Convocar(quantidadePoder);
        }
    }
}