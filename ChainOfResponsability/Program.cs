﻿using System;

namespace ChainOfResponsability
{
    class Program
    {
        static void Main(string[] args)
        {
            var pA = new PersonagemA();
            var pB = new PersonagemB();
            var pC = new PersonagemC();
            
            pA.DefinirSucessor(pB);
            pB.DefinirSucessor(pC);

            int[] poderes = {5, 8, 15, 20, 18, 3, 27, 20};
            
            foreach (var poder in poderes)
            {
                pA.Convocar(poder);
            }

            Console.ReadKey();
        }
    }
}