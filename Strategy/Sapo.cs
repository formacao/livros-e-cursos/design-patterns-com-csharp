﻿namespace Strategy
{
    public class Sapo : IAjuda
    {
        public string Ajudar(Ajuda pedido) => "Sou um sapo e posso te ajudar pular mais alto";
        
    }
}