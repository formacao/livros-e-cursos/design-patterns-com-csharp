﻿namespace Strategy
{
    public class Ajuda
    {
        private IAjuda _ajuda;
        public Ajuda(IAjuda ajuda)
        {
            _ajuda = ajuda;
        }

        public string Ajudar() => _ajuda.Ajudar(this);
    }
}