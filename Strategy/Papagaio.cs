﻿namespace Strategy
{
    public class Papagaio : IAjuda
    {
        public string Ajudar(Ajuda pedido) => "Sou um papagaio e posso te ajudar a voar";
    }
}