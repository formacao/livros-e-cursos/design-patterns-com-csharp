﻿using System;

namespace Strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            var papagaio = new Ajuda(new Papagaio());
            var sapo = new Ajuda(new Sapo());
            
            Console.WriteLine(sapo.Ajudar());
            Console.WriteLine(papagaio.Ajudar());

            Console.ReadKey();
        }
    }
}