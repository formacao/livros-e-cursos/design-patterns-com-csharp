﻿using System;

namespace Prototype
{
    public  class NuvemConcreta : NuvemMolde
    {
        private string _corPreenchimento;
        private string _corBorda;

        
        public NuvemConcreta(string corPreenchimento, string corBorda)
        {
            _corPreenchimento = corPreenchimento;
            _corBorda = corBorda;
        }
        
        public override NuvemMolde Clone()
        {
            Console.WriteLine($"Nuvem clonada com cor {_corPreenchimento} e borda {_corBorda}");
            return MemberwiseClone() as NuvemMolde;
        }
    }
}