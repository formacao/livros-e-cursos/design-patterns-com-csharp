﻿using System.Collections.Generic;

namespace Prototype
{
    public class GerenciadorNuvens
    {
        private Dictionary<string, NuvemMolde> _nuvens = new Dictionary<string, NuvemMolde>();

        public NuvemMolde this[string key]
        {
            get => _nuvens[key];
            set => _nuvens.Add(key, value);
        }
    }
}