﻿using System;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            var gerenciador = new GerenciadorNuvens();
            
            gerenciador["padrão"] = new NuvemConcreta("branco", "azul");
            gerenciador["personalizada"] = new NuvemConcreta("branco", "laranja");

            var um = gerenciador["padrão"].Clone();
            var dois = gerenciador["padrão"].Clone();
            var três = gerenciador["personalizada"].Clone();

            Console.ReadKey();
        }
    }
}