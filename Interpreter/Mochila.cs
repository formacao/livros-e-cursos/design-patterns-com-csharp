﻿using System;

namespace Interpreter
{
    public class Mochila : IExpressão
    {
        private readonly IFerramenta _ferramentaPrincipal;
        private readonly IFerramenta _ferramentaSecundária;
        private readonly IArmamento _armamento;
        
        public Mochila(IFerramenta ferramentaPrincipal, IFerramenta ferramentaSecundária, IArmamento armamento)
        {
            _ferramentaPrincipal = ferramentaPrincipal;
            _ferramentaSecundária = ferramentaSecundária;
            _armamento = armamento;
        }

        
        public void Interpretar(Contexto contexto)
        {
            contexto.Conteúdo += "Abrindo mochila... \n";
            
            _armamento.Interpretar(contexto);
            contexto.Conteúdo += " - 1º Ferramenta";
            
            _ferramentaPrincipal.Interpretar(contexto);
            contexto.Conteúdo += " - 2º Ferramenta";
            
            _ferramentaSecundária.Interpretar(contexto);
            contexto.Conteúdo += "\n... Fechando mochila";
            
            Console.WriteLine(contexto.Conteúdo);
        }
    }
}