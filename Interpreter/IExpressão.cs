﻿namespace Interpreter
{
    public interface IExpressão
    {
        void Interpretar(Contexto contexto);
    }
}