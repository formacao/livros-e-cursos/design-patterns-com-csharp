﻿namespace Interpreter
{
    public class Bússola : IFerramenta
    {
        public void Interpretar(Contexto contexto)
        {
            contexto.Conteúdo += string.Format(" {0} ", "Bússola");
        }
    }
}