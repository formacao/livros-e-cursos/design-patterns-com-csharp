﻿using System;

namespace Interpreter
{
    class Program
    {
        static void Main(string[] args)
        {
            var mochila = new Mochila(new Corda(), new Binóculos(), new ArcoFlecha());
            mochila.Interpretar(new Contexto());
            Console.ReadKey();
        }
    }
}