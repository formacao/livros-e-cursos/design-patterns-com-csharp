﻿namespace Interpreter
{
    public class Binóculos : IFerramenta
    {
        public void Interpretar(Contexto contexto)
        {
            contexto.Conteúdo += string.Format(" {0} ", "Binóculos");
        }
    }
}