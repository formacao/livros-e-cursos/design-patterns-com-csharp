﻿using System;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            var	jogador1	=	Singleton.Instância;
            jogador1.Mensagem("Jogador	1:	A	bola	está	comigo	no	meio	do	campo.");

            var	jogador2	=	Singleton.Instância;
            jogador2.Mensagem("Jogador	2:	recebeu	a	bola.");
            
            var	jogador3	=	Singleton.Instância;
            jogador3.Mensagem("Jogador	3:	recebeu	o	lançamento	na	linha	de	fundo.");
            
            Console.ReadKey();
        }
    }
}