﻿using System;

namespace Singleton
{
    public sealed class Singleton
    {
        private static Singleton _instância = null;

        public static Singleton Instância
        {
            get
            {
                if (_instância == null)
                {
                    _instância = new Singleton();
                    Console.WriteLine("Bola em Jogo");
                }

                return _instância;
            }
        }

        public void Mensagem(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}