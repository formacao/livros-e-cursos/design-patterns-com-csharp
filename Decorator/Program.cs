﻿using System;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            var armadura = new ArmaduraPadrão();
            var capacete = new Capacete(armadura);
            var espada = new Espada(capacete);
            
            Console.WriteLine(espada.Descrição);
        }
    }
}