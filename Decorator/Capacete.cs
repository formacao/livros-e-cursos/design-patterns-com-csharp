﻿namespace Decorator
{
    public class Capacete : DecoratorArmadura
    {
        public Capacete(MoldeArmadura armadura) : base(armadura, "Capacete")
        {
        }
    }
}