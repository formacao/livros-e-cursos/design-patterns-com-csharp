﻿namespace Decorator
{
    public abstract  class DecoratorArmadura : MoldeArmadura
    {
        public MoldeArmadura Armadura { get; }
        public override string Descrição => $"{Armadura.Descrição},  {base.Descrição}";

        protected DecoratorArmadura(MoldeArmadura armadura, string descrição) : base(descrição)
        {
            Armadura = armadura;
        }
    }
}