﻿namespace Decorator
{
    public class Espada : DecoratorArmadura
    {
        public Espada(MoldeArmadura armadura) : base(armadura, "Espada muito forte")
        {
        }
    }
}