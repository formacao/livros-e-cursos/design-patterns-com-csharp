﻿namespace Decorator
{
    public abstract class MoldeArmadura
    {
        public virtual string Descrição { get; }

        protected MoldeArmadura()
        {
            Descrição = "Armadura do personagem abstrata";
        }
        
        protected MoldeArmadura(string descrição)
        {
            Descrição = descrição;  
        }
    }
}