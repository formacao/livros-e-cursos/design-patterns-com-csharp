﻿using System;

namespace Proxy
{
    public class FaseProxy : IFase
    {
        private Fase _fase;
        private readonly string _password = "123";
        
        
        public string Jogar() => _fase?.Jogar() ?? "Informe o password para jogar a fase";

        public void InformarPassword(string password)
        {
            _fase = password == _password ? new Fase() : null;
        }
    }
}