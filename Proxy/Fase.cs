﻿namespace Proxy
{
    public class Fase : IFase
    {
        public string Jogar() => "Você está de volta a fase do jogo.";
    }
}