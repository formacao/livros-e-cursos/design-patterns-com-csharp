﻿using System;

namespace Proxy
{
    class Program
    {
        static void Main(string[] args)
        {
            var proxy = new FaseProxy();
            
            Console.WriteLine(proxy.Jogar());
            
            proxy.InformarPassword("41548668");
            Console.WriteLine(proxy.Jogar());
            
            proxy.InformarPassword("123");
            Console.WriteLine(proxy.Jogar());
        }
    }
}