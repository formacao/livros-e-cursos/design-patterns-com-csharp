﻿using System;

namespace Command
{
    public class SimplesComando : ICommand
    {
        private readonly string _solicitação;
        public SimplesComando(string solicitação)
        {
            _solicitação = solicitação;
        }

        public void Executar()
        {
           Console.WriteLine("Executando um simples comando de {0}", _solicitação);
        }
    }
}