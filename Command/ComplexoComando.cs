﻿namespace Command
{
    public class ComplexoComando : ICommand
    {
        private Receiver _receiver;
        private string _a;
        private string _b;

        public ComplexoComando(Receiver receiver, string a, string b)
        {
            _receiver = receiver;
            _a = a;
            _b = b;
        }

        public void Executar()
        {
            _receiver.PrimeiroPedido(_a);
            _receiver.SegundoPedodo(_b);
        }
    }
}