﻿using System.Collections.Generic;

namespace Command
{
    public class Controle
    {
        private readonly List<ICommand> _comandos;

        public Controle()
        {
            _comandos = new List<ICommand>();
        }

        public void AdicionarComando(ICommand comando) => _comandos.Add(comando);
        public void Fazer() => _comandos.ForEach(x => x.Executar());
    }
}