﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Command
{
    class Program
    {
        static void Main(string[] args)
        {
            var controle = new Controle();
            controle.AdicionarComando(new SimplesComando("Dizer oi"));

            var receiver = new Receiver();
            controle.AdicionarComando(new ComplexoComando(receiver, "Abastecer carro", "calibrar pneus do carro"));
            
            controle.Fazer();
            Console.ReadKey();
        }
    }
}