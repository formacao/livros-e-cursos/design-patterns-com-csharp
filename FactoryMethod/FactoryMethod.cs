﻿using System;
using System.Diagnostics;
using FactoryMethod.Personagens;
using FactoryMethod.Personagens.Interfaces;

namespace FactoryMethod
{
    public class FactoryMethod
    {
        public IPersonagem EscolherPersonagem(string nome) => nome switch
        {
            "Liu Kang" => new LiuKang(),
            "Scorpion" => new Scorpion(),
            "Sub Zero" => new SubZero(),
            _ =>throw new InvalidOperationException("Personagem Inexistente")
        };
    }
}