﻿namespace FactoryMethod.Personagens.Interfaces
{
    public interface IPersonagem
    {
        void Escolhido();
    }
}