﻿using System;
using FactoryMethod.Personagens.Interfaces;

namespace FactoryMethod.Personagens
{
    public class Scorpion : IPersonagem
    {
        public void Escolhido()
        {
            Console.WriteLine("Scorpion");
        }
    }
}