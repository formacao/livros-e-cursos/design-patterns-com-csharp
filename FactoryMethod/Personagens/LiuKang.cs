﻿using System;
using FactoryMethod.Personagens.Interfaces;

namespace FactoryMethod.Personagens
{
    public class LiuKang : IPersonagem
    {
        public void Escolhido()
        {
            Console.WriteLine("Liu Kang");
        }
    }
}