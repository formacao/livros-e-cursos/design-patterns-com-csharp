﻿using System;
using FactoryMethod.Personagens.Interfaces;

namespace FactoryMethod.Personagens
{
    public class SubZero: IPersonagem
    {
        public void Escolhido()
        {
            Console.WriteLine("Sub Zero");
        }
    }
}