﻿using System;

namespace FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            var fm = new FactoryMethod();
            
            Console.WriteLine("Liu Kang | Sub Zero | Scorpion");
            Console.WriteLine();

            var escolha = Console.ReadLine();
            var personagem = fm.EscolherPersonagem(escolha);
            
            Console.WriteLine("Você vai jogar com:");
            personagem.Escolhido();

            Console.ReadKey();
        }
    }
}