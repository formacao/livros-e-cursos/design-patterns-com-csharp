﻿using System;

namespace State
{
    public abstract class State
    {
        public abstract void Ação(Contexto contexto);
    }
}