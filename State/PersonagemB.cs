﻿namespace State
{
    public class PersonagemB : State
    {
        public override void Ação(Contexto contexto) => contexto.Estado = new PersonagemA();
    }
}