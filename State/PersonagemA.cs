﻿namespace State
{
    public class PersonagemA : State
    {
        public override void Ação(Contexto contexto) => contexto.Estado = new PersonagemB();
    }
}