﻿using System;

namespace State
{
    public class Contexto
    {
        private State _estado;

        public State Estado
        {
            get => _estado;
            set
            {
                _estado = value;
                Console.WriteLine("Agora o comportamento é de {0}", _estado.GetType().Name);
            }
        }

        public Contexto(State estado)
        {
            _estado = estado;
        }

        public void Trocar() => _estado.Ação(this);
    }
}