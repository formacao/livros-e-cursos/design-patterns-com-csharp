﻿using System;

namespace State
{
    class Program
    {
        static void Main(string[] args)
        {
            var contexto = new Contexto(new PersonagemA());
            
            contexto.Trocar();
            contexto.Trocar();
            contexto.Trocar();
            contexto.Trocar();

            Console.ReadKey();
        }
    }
}