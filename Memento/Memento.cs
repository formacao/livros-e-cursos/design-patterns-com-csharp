﻿namespace Memento
{
    public class Memento
    {
        public string Estado { get; }
        public Memento(string estado)
        {
            Estado = estado;
        }
    }
}