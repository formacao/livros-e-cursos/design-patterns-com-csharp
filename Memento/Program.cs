﻿using System;

namespace Memento
{
    class Program
    {
        static void Main(string[] args)
        {
            var ação = new Ação();
            ação.Estado = "play";

            var armazena = new Armazena();
            armazena.Memento = ação.CriarMemento();

            ação.Estado = "pause";
            ação.RestaurarMemento(armazena.Memento);

            Console.ReadKey();
        }
    }
}