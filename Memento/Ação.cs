﻿using System;

namespace Memento
{
    public class Ação
    {
        private string _estado;

        public string Estado
        {
            get => _estado;
            set
            {
                _estado = value;
                Console.WriteLine("Estado do jogo = {0}", _estado);
            }
        }

        public Memento CriarMemento() => new Memento(Estado);

        public void RestaurarMemento(Memento memento)
        {
            Console.WriteLine("Restaurando Estado....");
            Estado = memento.Estado;
        }
    }
}